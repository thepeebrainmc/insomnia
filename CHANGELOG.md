# Changelog
All notable changes to this project will be documented in this file.

## [1.1.0] - 2023-01-02

### Added

- Game rule `sleepCheckOverworldOnly`

### Changed

- Game rule `allowSleepingDuringDay` to `allowSleepDuringDay`

### Fixed

- Game rule category formatting

## [1.0.1] - 2022-12-11

### Fixed

- Rogue return value from disabling sleep altogether

## [1.0.0] - 2022-12-10

### Added

- Sleep-related game rules
  - `allowNearbyMonsters`
  - `allowResettingTime`
  - `allowSettingSpawn`
  - `allowSleeping`
  - `allowSleepingDuringDay`