package com.gitlab.thepeebrain.sleepconfig;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.entity.event.v1.EntitySleepEvents;
import net.fabricmc.fabric.api.gamerule.v1.CustomGameRuleCategory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SleepConfig
		implements ModInitializer
{
	public static final String MOD_ID = "sleep_config";

	public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

	public static final CustomGameRuleCategory GAME_RULE_CATEGORY;

	public static final GameRules.Key<GameRules.BooleanRule> ALLOW_NEARBY_MONSTERS;
	public static final GameRules.Key<GameRules.BooleanRule> ALLOW_RESETTING_TIME;
	public static final GameRules.Key<GameRules.BooleanRule> ALLOW_SETTING_SPAWN;
	public static final GameRules.Key<GameRules.BooleanRule> ALLOW_SLEEP_DURING_DAY;
	public static final GameRules.Key<GameRules.BooleanRule> ALLOW_SLEEPING;
	public static final GameRules.Key<GameRules.BooleanRule> CHECK_OVERWORLD_ONLY;

	@Override
	public void onInitialize() {}

	static
	{
		GAME_RULE_CATEGORY = new CustomGameRuleCategory(new Identifier(MOD_ID, "game_rule_category"),
				Text.translatable("key.categories.sleep").formatted(Formatting.BOLD, Formatting.YELLOW));

		GameRules.Type<GameRules.BooleanRule> trueRule = GameRuleFactory.createBooleanRule(true);
		GameRules.Type<GameRules.BooleanRule> falseRule = GameRuleFactory.createBooleanRule(false);

		ALLOW_NEARBY_MONSTERS = GameRuleRegistry.register("allowNearbyMonsters", GAME_RULE_CATEGORY, falseRule);
		ALLOW_RESETTING_TIME = GameRuleRegistry.register("allowResettingTime", GAME_RULE_CATEGORY, trueRule);
		ALLOW_SETTING_SPAWN = GameRuleRegistry.register("allowSettingSpawn", GAME_RULE_CATEGORY, trueRule);
		ALLOW_SLEEP_DURING_DAY = GameRuleRegistry.register("allowSleepDuringDay", GAME_RULE_CATEGORY, falseRule);
		ALLOW_SLEEPING = GameRuleRegistry.register("allowSleeping", GAME_RULE_CATEGORY, trueRule);
		CHECK_OVERWORLD_ONLY = GameRuleRegistry.register("sleepCheckOverworldOnly", GAME_RULE_CATEGORY, falseRule);

		EntitySleepEvents.ALLOW_NEARBY_MONSTERS.register(SleepConfig::allowNearbyMonsters);
		EntitySleepEvents.ALLOW_RESETTING_TIME.register(SleepConfig::allowResettingTime);
		EntitySleepEvents.ALLOW_SETTING_SPAWN.register(SleepConfig::allowSettingSpawn);
		EntitySleepEvents.ALLOW_SLEEP_TIME.register(SleepConfig::allowSleepDuringDay);
		EntitySleepEvents.ALLOW_SLEEPING.register(SleepConfig::allowSleeping);
	}

	private static ActionResult allowNearbyMonsters(PlayerEntity player, BlockPos sleepingPos, boolean vanillaResult)
	{
		World world = player.getWorld();
		GameRules gameRules = world.getGameRules();
		return isCheckedWorld(world, gameRules) && gameRules.getBoolean(ALLOW_NEARBY_MONSTERS)
				? ActionResult.SUCCESS : ActionResult.PASS;
	}

	private static boolean allowResettingTime(PlayerEntity player)
	{
		World world = player.getWorld();
		GameRules gameRules = world.getGameRules();
		return !isCheckedWorld(world, gameRules) || gameRules.getBoolean(ALLOW_RESETTING_TIME);
	}

	private static boolean allowSettingSpawn(PlayerEntity player, BlockPos sleepingPos)
	{
		World world = player.getWorld();
		GameRules gameRules = world.getGameRules();
		return !isCheckedWorld(world, gameRules) || gameRules.getBoolean(ALLOW_SETTING_SPAWN);
	}

	private static ActionResult allowSleepDuringDay(PlayerEntity player, BlockPos sleepingPos, boolean vanillaResult)
	{
		World world = player.getWorld();
		GameRules gameRules = world.getGameRules();
		return isCheckedWorld(world, gameRules) && gameRules.getBoolean(ALLOW_SLEEP_DURING_DAY)
				? ActionResult.SUCCESS : ActionResult.PASS;
	}

	private static PlayerEntity.SleepFailureReason allowSleeping(PlayerEntity player, BlockPos sleepingPos)
	{
		World world = player.getWorld();
		GameRules gameRules = world.getGameRules();
		return !isCheckedWorld(world, gameRules) || gameRules.getBoolean(ALLOW_SLEEPING)
				? null : PlayerEntity.SleepFailureReason.OTHER_PROBLEM;
	}

	private static boolean isCheckedWorld(World world, GameRules gameRules)
	{
		return !gameRules.getBoolean(CHECK_OVERWORLD_ONLY)
				|| world.getRegistryKey().getValue().equals(World.OVERWORLD.getValue());
	}
}
