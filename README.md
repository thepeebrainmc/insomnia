# Sleep Config

Adds sleep-related game rules.

---

- [**Downloads**](https://www.curseforge.com/minecraft/mc-mods/thepeebrain-sleep-config/files)
- [**Wiki**](https://gitlab.com/thepeebrainmc/sleep_config/-/wikis/home)

**Required Mods**

- [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api "Fabric API")